﻿using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Data.Sqlite;
using MySql.Data.MySqlClient;
using RaceTracker.Models.LocalData;
using RaceTracker.Models.MySQL;

namespace RaceTracker.Models
{
    public class DataHandler
    {
        public bool IsInited { get; set; }
        public List<Instance> TrackedInstances { get; set; }
        public DataHandler()
        {
            TrackedInstances = new List<Instance>();

            InitSqlite();
            RefreshMysql();

            IsInited = true;
        }

        public void AddGuidMapping(string playerName, string guid)
        {
            var pgObj = new PlayerGuid();
            pgObj.PlayerName = playerName;
            pgObj.CharGuid = guid;
            using (var cnn = new SqliteConnection("Data Source=" + SqliteDbFile))
            {
                cnn.Open();
                var existingMapping = cnn.Query<PlayerGuid>("SELECT * FROM PlayerGuid").Any(qq => qq.PlayerName == playerName);
                if (!existingMapping)
                {
                    cnn.Insert(pgObj);
                }
            }
        }

        private string? GetNameOrEmpty(GroupCompletedChallenge compChall)
        {
            return GetNameOrEmpty(compChall.guid_1, compChall.guid_2, compChall.guid_3, compChall.guid_4, compChall.guid_5);
        }

        private string? GetNameOrEmpty(string guid1, string guid2, string guid3, string guid4, string guid5)
        {

            using (var cnn = new SqliteConnection("Data Source=" + SqliteDbFile))
            {
                cnn.Open();
                var mappings = cnn.GetAll<PlayerGuid>();
                var m = mappings.FirstOrDefault(qq => qq.CharGuid == guid1 || qq.CharGuid == guid2 || qq.CharGuid == guid3 || qq.CharGuid == guid4 || qq.CharGuid == guid5);
                return m?.PlayerName;
            }
        }

        private static string SqliteDbFile
        {
            get { return Environment.CurrentDirectory + Path.DirectorySeparatorChar + "tracker.sqlite"; }
        }
        private void InitSqlite()
        {
            if (!File.Exists(SqliteDbFile))
            {
                using (var cnn = new SqliteConnection("Data Source=" + SqliteDbFile))
                {
                    cnn.Open();
                    cnn.Execute(
                        @"create table PlayerGuid (
                        ID                                  integer primary key AUTOINCREMENT,
                        PlayerName                          varchar(200) not null,
                        CharGuid                            varchar(200) not null
                        )");
                    cnn.Execute(
                        @"create table MapName (
                        MapId                               integer primary key,
                        InstanceName                        varchar(200) not null
                        )");

                    var pp = cnn.Query<MapName>("SELECT * FROM MapName");
                    if (pp == null || pp.Count() == 0)
                    {
                        var mm = new MapName();
                        mm.MapId = 959;
                        mm.InstanceName = "Shado-Pan Monastery";
                        cnn.Insert(mm);
                        mm = new MapName();
                        mm.MapId = 960;
                        mm.InstanceName = "Temple of the Jade Serpent";
                        cnn.Insert(mm);
                        mm = new MapName();
                        mm.MapId = 961;
                        mm.InstanceName = "Stormstout Brewery";
                        cnn.Insert(mm);
                        mm = new MapName();
                        mm.MapId = 962;
                        mm.InstanceName = "Gate of the Setting Sun";
                        cnn.Insert(mm);
                        mm = new MapName();
                        mm.MapId = 994;
                        mm.InstanceName = "Mogu'shan Palace";
                        cnn.Insert(mm);
                        mm = new MapName();
                        mm.MapId = 1001;
                        mm.InstanceName = "Scarlet Halls";
                        cnn.Insert(mm);
                        mm = new MapName();
                        mm.MapId = 1004;
                        mm.InstanceName = "Scarlet Monasteryt";
                        cnn.Insert(mm);
                        mm = new MapName();
                        mm.MapId = 1007;
                        mm.InstanceName = "Scholomance";
                        cnn.Insert(mm);
                        mm = new MapName();
                        mm.MapId = 1011;
                        mm.InstanceName = "Siege of Niuzao Temple";
                        cnn.Insert(mm);
                    }
                }
            }

            using (var cnn = new SqliteConnection("Data Source=" + SqliteDbFile))
            {
                cnn.Open();
                var mms = cnn.GetAll<MapName>();
                foreach (var item in mms)
                {
                    Instance i = new Instance();
                    i.Name = item.InstanceName;
                    i.ID = item.MapId;
                    TrackedInstances.Add(i);
                }
            }

        }

        private void RefreshMysql()
        {
            var gcsList = new List<GroupCompletedChallenge>();
            using (var connection = new MySqlConnection("Server=;Database=;Uid=;Pwd=;"))
            {
                gcsList = connection.Query<GroupCompletedChallenge>("SELECT map, attemptId, completion_time, completion_date, group_members, member_1_guid,member_2_guid,member_3_guid,member_4_guid,member_5_guid FROM group_completed_challenges").ToList();
            }
            foreach (var compChall in gcsList)
            {
                var pName = GetNameOrEmpty(compChall);
                pName = pName ?? "<Unknown>";
                Attempt a = new Attempt();
                a.PlayerName = pName;
                a.TimeSeconds = compChall.completion_time;
                TrackedInstances.FirstOrDefault(w => w.ID == compChall.map)?.Attempts.Add(a);
            }
            // "SELECT map, attemptId, completion_time, completion_date, group_members, member_1_guid,member_2_guid,member_3_guid,member_4_guid,member_5_guid FROM group_completed_challenges";
        }


    }
}
