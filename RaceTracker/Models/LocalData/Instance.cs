﻿namespace RaceTracker.Models.LocalData
{
    public class Instance
    {
        private List<Attempt> attempts = new List<Attempt>();

        public int ID { get; set; }
        public string Name { get; set; }
        public List<Attempt> Attempts
        {
            get => attempts.OrderBy(qq => qq.TimeSeconds).ToList();
            set => attempts = value;
        }
    }
}
