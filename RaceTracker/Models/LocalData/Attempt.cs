﻿namespace RaceTracker.Models.LocalData
{
    public class Attempt
    {
        public int ID { get; set; }
        public string PlayerName { get; set; }
        public int TimeSeconds { get; set; }
    }
}
