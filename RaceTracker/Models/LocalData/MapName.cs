﻿using Dapper.Contrib.Extensions;

namespace RaceTracker.Models.LocalData
{
    [Table("MapName")]
    public class MapName
    {
        [ExplicitKey]
        public int MapId { get; set; }
        public string InstanceName { get; set; }
    }
}
