﻿using Dapper.Contrib.Extensions;

namespace RaceTracker.Models.LocalData
{
    [Table("PlayerGuid")]
    public class PlayerGuid
    {
        [Key]
        public int ID { get; set; }
        public string PlayerName { get; set; }
        public string CharGuid { get; set; }
    }
}
