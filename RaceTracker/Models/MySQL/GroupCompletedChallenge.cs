﻿namespace RaceTracker.Models.MySQL
{
    public class GroupCompletedChallenge
    {
        public int map { get; set; }
        public int attemptId { get; set; }
        public int completion_time { get; set; }
        public int completion_date { get; set; }
        public byte group_members { get; set; }

        public int member_1_guid { get; set; }
        public int member_2_guid { get; set; }
        public int member_3_guid { get; set; }
        public int member_4_guid { get; set; }
        public int member_5_guid { get; set; }

        public string guid_1 { get => $"{member_1_guid}"; }
        public string guid_2 { get => $"{member_2_guid}"; }
        public string guid_3 { get => $"{member_3_guid}"; }
        public string guid_4 { get => $"{member_4_guid}"; }
        public string guid_5 { get => $"{member_5_guid}"; }
    }
}
